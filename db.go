package badgersimple

import (
	"encoding/json"
	"reflect"
	"time"

	"github.com/dgraph-io/badger"
)

const (
	gcDiscardRatio  = 0.7
	gcRunInterval   = 1 * time.Hour
	gcSleepInterval = 1 * time.Minute
)

// DB database
type DB struct {
	closed  bool
	options *badger.Options
	db      *badger.DB
}

// New creates a new database reference
func New(path string, logLevel LogLevel) (*DB, error) {
	l := newLogger(logLevel)

	options := badger.DefaultOptions(path)
	options = options.WithCompactL0OnClose(true)
	options = options.WithLogger(l)

	badgerDB, err := badger.Open(options)
	if err != nil {
		return nil, err
	}

	db := DB{closed: false, db: badgerDB, options: &options}

	go func() {
		lastGCRun := time.Now().Add(gcSleepInterval)

		for {
			if db.closed {
				break
			}

			if time.Since(lastGCRun) >= gcRunInterval {
				lastGCRun = time.Now()

				_ = db.RunGC()
			}

			time.Sleep(gcSleepInterval)
		}
	}()

	return &db, nil
}

// Close closes a database
func (db *DB) Close() error {
	db.closed = true

	return db.db.Close()
}

// RunGC run garbage collection
func (db *DB) RunGC() error {
	var err error = nil

	for err == nil {
		if db.closed {
			return nil
		}

		err = db.db.RunValueLogGC(gcDiscardRatio)
	}

	return nil
}

// Exist check if a specific key exists
func (db *DB) Exist(key string) bool {
	err := db.db.View(func(txn *badger.Txn) error {
		_, err := txn.Get([]byte(key))
		return err
	})

	return err == nil
}

// Get load entry for given key
func (db *DB) Get(key string, value interface{}) error {
	return db.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(key))
		if err != nil {
			return err
		}

		return item.Value(func(val []byte) error {
			return json.Unmarshal(val, value)
		})
	})
}

// Set entry for given key
func (db *DB) Set(key string, value interface{}) error {
	bytes, err := json.Marshal(value)
	if err != nil {
		return err
	}

	return db.db.Update(func(txn *badger.Txn) error {
		return txn.Set([]byte(key), bytes)
	})
}

// SetWithTTL set an entry for given key with TTL
func (db *DB) SetWithTTL(key string, value interface{}, ttl time.Duration) error {
	bytes, err := json.Marshal(value)
	if err != nil {
		return err
	}

	return db.db.Update(func(txn *badger.Txn) error {
		return txn.SetEntry(badger.NewEntry([]byte(key), bytes).WithTTL(ttl))
	})
}

// Delete removes a key from the db
func (db *DB) Delete(key string) error {
	return db.db.Update(func(txn *badger.Txn) error {
		return txn.Delete([]byte(key))
	})
}

// Prefix get all entries with given prefix
func (db *DB) Prefix(prefix string, resultType interface{}, fn func(key string, value interface{}) error) error {
	valueType := reflect.TypeOf(resultType).Elem()

	return db.db.View(func(txn *badger.Txn) error {
		bytePrefix := []byte(prefix)

		iteratorOptions := badger.DefaultIteratorOptions
		iteratorOptions.Prefix = bytePrefix

		it := txn.NewIterator(iteratorOptions)
		defer it.Close()

		for it.Seek(bytePrefix); it.ValidForPrefix(bytePrefix); it.Next() {
			item := it.Item()

			err := item.Value(func(v []byte) error {
				value := reflect.New(valueType).Interface()

				err := json.Unmarshal(v, value)
				if err != nil {
					return err
				}

				return fn(string(item.Key()), value)
			})
			if err != nil {
				return err
			}
		}

		return nil
	})
}
