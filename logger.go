package badgersimple

import (
	"log"
	"os"
)

// LogLevel log level
type LogLevel uint8

// LogLevels
const (
	LogLevelDebug   LogLevel = 0
	LogLevelInfo    LogLevel = 1
	LogLevelWarning LogLevel = 2
	LogLevelError   LogLevel = 3
	LogLevelNone    LogLevel = 4
)

type logger struct {
	log   *log.Logger
	level LogLevel
}

func newLogger(level LogLevel) *logger {
	return &logger{log: log.New(os.Stderr, "badger ", log.LstdFlags), level: level}
}

func (l *logger) Errorf(f string, v ...interface{}) {
	if l.level <= LogLevelError {
		l.log.Printf("ERROR: "+f, v...)
	}
}

func (l *logger) Warningf(f string, v ...interface{}) {
	if l.level <= LogLevelWarning {
		l.log.Printf("WARNING: "+f, v...)
	}
}

func (l *logger) Infof(f string, v ...interface{}) {
	if l.level <= LogLevelInfo {
		l.log.Printf("INFO: "+f, v...)
	}
}

func (l *logger) Debugf(f string, v ...interface{}) {
	if l.level <= LogLevelDebug {
		l.log.Printf("ERROR: "+f, v...)
	}
}
