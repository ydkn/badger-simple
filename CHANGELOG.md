# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2019-11-11
### Added
- Exist: check if entry exists
- Get: get entry for key
- Set: set entry for key
- SetWithTTL: set entry for key with TTL
- Delete: removes an entry from the db
- Prefix: perform a prefix search
- Logging configuration
- RunGC: run garbage collection
