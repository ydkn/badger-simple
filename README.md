# Simple Badger

Provides a simpler interface for the key-value store badger (https://github.com/dgraph-io/badger).

## Usage

```go
db, _ := badger.New("/tmp/db")

var value1 myStruct
var value2 myStruct

_ = db.Set("foo", &value1)

_ = db.Get("foo", &value2)

_ = db.Prefix("myPrefix", &myStruct{}, func(key string, value interface{}) error {
	castValue := value.(*myStruct)

  return nil
})
```
